# Depth-3 Circuits


## Description
Supplemental materials for the paper

Depth-Three Circuits for Inner Product and Majrotiy Functions

by K. Amano (2023)


1. Inner Product

- 2CNF-IP4-Size7.txt
- 2CNF-IP4Neg-Size7.txt
- 3CNF-IP4-Size4.txt
- 3CNF-IP4Neg-Size3.txt
- 3CNF-IP5-Size6.txt
- 3CNF-IP5Neg-Size5.txt

One line per one clause.

Each line consists of three integers that represent the index of CNF formula, the set of indices of positive literals and the set of indices of negative literals appeared in that clause.
  The second and thirds numbers are decimal numbers representing a subset of (x_1,...,x_n,y_1,...,y_n) in a binary fashion where the LSB is x_1 and the MSB is y_n.

  For example, for IP4, "3 17 36" represents that the third CNF formula (counting from zero) contains a clause (x_1 OR y_1 OR NOT(x_3) OR NOT(y_2)) (since 17 = 2^0 + 2^4 = "x_1 + y_1" and 36 = 2^2 + 2^5 = "x_3 + y_2").


2. Majority

- 3+CNF-Maj12-217.txt
-   3+CNF-Maj16-1314.txt
-   5+CNF-Maj16-4958.txt

  One line per one clause.

  Each line consists of two integers that represent the set of indices of positive literals and the set of indices of negative literals appeared in that clause.
  
  For example, "7 8" represents a clause (x1 OR x2 OR x3 OR NOT(x4)), and "0 5" represents a clause (NOT(x1) OR NOT(x3)).

We obtained these by an IP solver [1].

[1] Gurobi optimizer. https://www.gurobi.com/

